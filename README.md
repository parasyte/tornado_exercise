Tornado API Exercise
====================

A Tornado-based server that provides a simple API for managing user data.

Getting
-------

    $ git clone https://bitbucket.org/parasyte/tornado_exercise.git

Dependencies
------------

* [Python](http://www.python.org/) 2.7+
* [Tornado](http://www.tornadoweb.org/) 2.4+

Configuration
-------------

The `config.json` file contains some basic server configuration settings:

* port: Server will listen on this port. Default: 8888
* db: A file path for the SQLite3 database. Default: "db.sqlite"

DB installation
---------------

    $ touch db.sqlite
    $ sqlite db.sqlite <model/schema.sql

Running
-------

    $ python main.py

Connect to the server using any HTTP client:

http://localhost:8888/users

API
---

### GET /users

Returns a list of user records. When a valid `auth_id` is provided, the records will include private information. E.g. E-mail addresses.

#### Parameters:

- *auth_id* [OPTIONAL] - Authenticated session token

#### Example:

**http://localhost:8888/users**

    {
        "users" : [
            {
                "id" : 1,
                "first" : "Travis",
                "last" : "Beauvais"
            },
            {
                "id" : 2,
                "first" : "Mark",
                "last" : "Kinsella"
            },
        ]
    }


### GET /user

Returns a specific user record. When a valid `auth_id` is provided, the record will include private information. E.g. E-mail addresses.

#### Parameters:

- *id* - User id
- *auth_id* [OPTIONAL] - Authenticated session token

#### Example:

**http://localhost:8888/user?id=1**

    {
        "id" : 1,
        "first" : "Travis",
        "last" : "Beauvais"
    }


### POST /user

Create a new user record.

#### Parameters:

- *first* - User's first name
- *last* - User's last name
- *email* - User's E-mail address
- *auth_id* - Authenticated session token

#### Example:

**http://localhost:8888/user?first=Sean&last=Fanna&email=sean@chartboost.com&auth_id=b01d13d5fc0b1d51bbf3b3e466ff1e26cb1c5f2b**

    {
        "id" : 3,
        "first" : "Sean",
        "last" : "Fanna",
        "email" : "sean@chartboost.com"
    }

Hacking
-------

The server uses a SQLite3 "model" module that provides a simple REST-like API
with GET, PUT, and POST methods. It can be extended to use other DB backends
by replacing this module with another that implements the same API.

Additional resources can be added by extending resources.DefaultHandler and
adding the new resource class to the Application declaration in main.py

Alert onlookers will notice this Tornado project does not handle requests
asynchronously. That's a limitation of the implemented API. If, for example, the
API was extended to fetch RSS or Atom feeds, those could be handled
asynchronously.

A new model implementation is actually required for scaling; SQLite is good for
a first development step. For a production environment, you will probably want a
data store that does really well with the "AP" parts of the CAP theorem. And
because such data stores are *Eventually consistent* by nature, accessing them
can be made asynchronous.

When data access is handled asynchronously, reads should be handled by opening
only one asynchronous request per unique resource. Additional requests for that
resource should be placed onto a queue until the result comes back. This will
keep your Tornado server from destroying your data store with tons of
asynchronous read requests for the same data.
