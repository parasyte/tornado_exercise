import json

class loaded:
    """
    A static class that acts as a singleton.
    """
    data = None


def load(file):
    """
    Load a JSON file into the singleton.
    """
    loaded.data = json.loads(open(file).read())
    return loaded.data
