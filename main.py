#!/usr/bin/env python

import tornado.web
import tornado.ioloop

import resources
import resources.user
import resources.users

import config
conf = config.load("config.json")

import model.sqlite
model.sqlite.open(conf["db"])


application = tornado.web.Application([
    (r"/user/?", resources.user.Handler),
    (r"/users/?", resources.users.Handler),
    (r"/.*", resources.DefaultHandler),
])

if __name__ == "__main__":
    application.listen(conf["port"])
    tornado.ioloop.IOLoop.instance().start()
