CREATE TABLE auth (
    id INTEGER PRIMARY KEY,
    key NOT NULL,
    value NOT NULL
);

INSERT INTO auth (key, value) VALUES (
    'b01d13d5fc0b1d51bbf3b3e466ff1e26cb1c5f2b',
    '{"first":"Jay","last":"Oster","email":"jay@kodewerx.org"}'
);

CREATE TABLE users (
    id INTEGER PRIMARY KEY,
    value NOT NULL
);

INSERT INTO users (value) VALUES (
    '{"first":"Travis","last":"Beauvais","email":"travis@chartboost.com"}'
);
INSERT INTO users (value) VALUES (
    '{"first":"Mark","last":"Kinsella","email":"mark@chartboost.com"}'
);
