import sqlite3
import json


class self:
    """
    A static class that acts as a singleton.
    """
    conn = None


def _encode(object):
    """
    Encode an object to a JSON string.
    """
    return json.dumps(object, separators=(',', ':'))

def _decode(row):
    """
    Safely decode a JSON string to an object
    """
    try:
        object = json.loads(row[1])
    except:
        return None

    object.update({ "id" : row[0] })
    return object


def open(db):
    """
    Open a SQLite database file.
    """
    self.conn = sqlite3.connect(db)

def close():
    """
    Close an opened database file.
    """
    self.conn.close()


def list(table, limit=10, offset=0):
    """
    Get a list of all objects in table.
    """
    # Yeah, I'm writing SQL statements again. ;(
    rows = self.conn.execute(
        '''SELECT id, value FROM {table} LIMIT ? OFFSET ?'''.format(
            table=table
        ),
        (limit, offset)
    )

    return [ _decode(row) for row in rows ]

def get(table, rowid, column="rowid"):
    """
    Get an object by (table, rowid)
    """
    row = self.conn.execute(
        '''SELECT id, value FROM {table} WHERE {column}=?'''.format(
            table=table,
            column=column
        ),
        (rowid,)
    ).fetchone()

    return _decode(row) if row else None

def put(table, rowid, value, column="rowid"):
    """
    Set an object by (table, rowid)
    """
    result = self.conn.execute(
        '''UPDATE {table} SET value=? WHERE {column}=?'''.format(
            table=table,
            column=column
        ),
        (_encode(value),)
    ).lastrowid

    self.conn.commit()
    return result

def post(table, value):
    """
    Create a new object.
    """
    result = self.conn.execute(
        '''INSERT INTO {table} (value) VALUES (?)'''.format(
            table=table
        ),
        (_encode(value),)
    ).lastrowid

    self.conn.commit()
    return result

def delete(table, rowid, column="rowid"):
    """
    Delete an object by (table, rowid)
    """
    result = self.conn.execute(
        '''DELETE FROM {table} WHERE {column}=?'''.format(
            table=table,
            column=column
        ),
        (rowid,)
    ).lastrowid

    self.conn.commit()
    return result
