import tornado.web

import model.sqlite as db


class DefaultHandler(tornado.web.RequestHandler):
    """
    A default request handler which returns HTTP 404 for all methods.
    Also supplies some useful methods for authentication.
    """

    def get(self):
        self.send_error(404)

    def post(self):
        self.send_error(404)

    def put(self):
        self.send_error(404)

    def delete(self):
        self.send_error(404)

    def head(self):
        self.send_error(404)

    def options(self):
        self.send_error(404)


    def write_error(self, status_code, **kwargs):
        """
        Respond with JSON error messages.
        """

        result = {
            "status_code" : status_code
        }
        if "exc_info" in kwargs:
            result["error"] = str(kwargs["exc_info"][1])

        self.write(result)


    def is_auth(self):
        """
        Check whether a request is authenticated with provided credentials.
        """
        auth_id = self.get_arguments("auth_id")
        if not auth_id:
            return None

        return db.get("auth", auth_id[0], "key")

    def auth(self):
        """
        Require authentication with provided credentials.
        """
        auth = db.get("auth", self.get_argument("auth_id"), "key")
        if not auth:
            self.send_error(401)
        return auth

    def privatize(self):
        """
        Strip private properties from result Dict.
        """

        private = (
            "email",
            "password"
        )

        def recurse(object):
            if isinstance(object, list):
                for value in object:
                    recurse(value)

            elif isinstance(object, dict):
                for key in private:
                    try:
                        del object[key]
                    except:
                        pass

                for value in object.itervalues():
                    recurse(value)

            return object

        if not self.is_auth():
            recurse(self.result)
