import tornado.web

import resources
import model.sqlite as db

class Handler(resources.DefaultHandler):
    """
    User resource handler.
    """

    def get(self):
        """
        Get user information by ID.
        """

        self.result = db.get("users", self.get_argument("id"))

        # Only authenticated requests get private info.
        self.privatize()

        self.write(self.result)


    def post(self):
        """
        Create a new user record.
        """

        # Authenticate.
        if not self.auth():
            return

        self.result = {
            "first" : self.get_argument("first"),
            "last"  : self.get_argument("last"),
            "email" : self.get_argument("email")
        }
        self.result["id"] = db.post("users", self.result)

        self.write(self.result)
