import tornado.web

import resources
import model.sqlite as db

class Handler(resources.DefaultHandler):
    """
    Users resource handler.
    """

    def get(self):
        """
        Get a list of all users in the system.
        """
        limit = self.get_arguments("limit")
        offset = self.get_arguments("offset")

        self.result = db.list(
            "users",
            limit[0] if limit else 10,
            offset[0] if offset else 0
        )

        self.privatize()

        self.write({
            "users" : self.result
        })
